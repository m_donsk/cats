import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    items: []
  },

  mutations: {
    setItems (state, items) {
      state.items = items
    }
  },

  actions: {
    fetchItems ({ commit }) {
      return fetch(`${process.env.BASE_URL}items.json`)
        .then(response => {
          if (response.ok) {
            return response.json()
          }

          throw new Error('Invalid response')
        })
        .then(items => commit('setItems', items))
    }
  }
})
